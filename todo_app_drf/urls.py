from rest_framework import routers
from todo_app_drf.views import MyTasksViewSet, TaskViewSet

my_task_router = routers.SimpleRouter()
my_task_router.register('', MyTasksViewSet, basename='my_tasks')
task_router = routers.SimpleRouter()
task_router.register('', TaskViewSet, basename='tasks')
