from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from account.models import Profile


class UserCreateForm(UserCreationForm):
    work_position = forms.CharField(required=True, max_length=100)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.work_position = self.cleaned_data['work_position']
        if commit:
            user.save()
        return user


class UserUpdateForm(forms.ModelForm):
    work_position = forms.CharField(required=True, max_length=100)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'work_position')

    def save(self, commit=True):
        user = super(UserUpdateForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.work_position = self.cleaned_data['work_position']
        if commit:
            user.save()
        return user